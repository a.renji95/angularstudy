import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-switches',
  templateUrl: './switches.component.html',
  styleUrls: ['./switches.component.scss']
})
export class SwitchesComponent implements OnInit {
  @Input() checked: Boolean = false;
  @Output('checkedChange') change = new EventEmitter<boolean>(); 
  constructor() { }

  ngOnInit() {
  }
  
  emitChangeValue(event) {
    this.change.emit(event.target.checked);
  }

}

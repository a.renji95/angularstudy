import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class ContactListComponent implements OnInit {
  message: string = "Thao <3<3<3";
  printable: boolean = false;
  tab: number = 1;
  static clickCounter = 0;
  constructor() { }

  ngOnInit() {
  }

  toggleMessage() {
    this.printable = !this.printable;
  }

  buttonTab(onTab) {
    this.tab = onTab;
  }

  contacts = [
    {
      id: 1,
      name: 'Hiep',
      job: 'Software Engineer',
      avatar: {
        url: 'https://via.placeholder.com/150',
        round: false
      }
    },
    {
      id: 2,
      name: 'Thao',
      job: 'Hiep\'s wife',
      avatar: {
        url: 'https://via.placeholder.com/150',
        round: false
      }
    }
  ];

  baseContact = {
    id: 3,
    name: 'Vy',
    job: 'Daughter',
    avatar: {
      url: 'https://via.placeholder.com/150',
      round: false
    }
  }

  getContact() {
    var count = ++ContactListComponent.clickCounter;
    if (count % 2 != 0) {
      this.contacts = this.contacts.concat([this.baseContact]);

    } else {
      this.contacts = this.contacts.splice(0, this.contacts.length - 1);
    }
  }

  // switchesValueChange(event: boolean, index) {
  //   this.contacts[index].avatar.round = event;
  // }
}

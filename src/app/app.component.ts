import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  title = 'App Component';
  messages: string[] = [];
  message = '';
  // onInput(event) {
  //   if (event.target.value) {
  //     this.messages.push(event.target.value);
  //   }
  // }
  onSend() {
    if (this.message) {
      this.messages.push(this.message);
      this.message = '';
    }
  }
}
